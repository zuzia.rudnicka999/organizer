package com.zuz.viewutil;

import com.zuz.model.SpendingCategoryEnum;
import javafx.scene.control.ListCell;

public class SpendingCategoryEnumCell extends ListCell<SpendingCategoryEnum> {

    @Override
    protected void updateItem(SpendingCategoryEnum item, boolean empty) {
        super.updateItem(item, empty);
        String name = null;
        // Format name
        if (item == null || empty) {
        } else {
            name = item.getName();
        }

        this.setText(name);
        //setGraphic(null);
    }
}
