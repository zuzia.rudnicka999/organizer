package com.zuz;

import com.zuz.views.MainWindow;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Organizer");
        MainWindow mainWindow = new MainWindow();
        window.setScene(mainWindow.makeScene());
        window.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
