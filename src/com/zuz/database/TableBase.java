package com.zuz.database;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.zuz.model.Id;

public abstract class TableBase<T extends Id> {

    private Long id = 0L;

    private List<T> items;

    public List<T> getItems() {
        return items;
    }

    public List<T> findAll() {
        return getItems();
    }

    public void save() {
        JSONArray itemJSONs = new JSONArray();
        itemJSONs.addAll(
            items.stream()
                .map(item -> mapToJSONObject(item))
                .collect(Collectors.toList())
        );

        //Write JSON file
        try (FileWriter file = new FileWriter(tableName() + ".json")) {
            file.write(itemJSONs.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(tableName() + ".json")) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray itemJSONs = (JSONArray) obj;

            //Iterate over employee array
            items = (List<T>) itemJSONs
                .stream()
                .map(itemJSON -> parseItemJSONObject((JSONObject) itemJSON))
                .collect(Collectors.toList());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        calculateMaxId();
    }

    protected abstract T parseItemJSONObject(JSONObject itemJSON);

    protected abstract String tableName();

    protected abstract JSONObject mapToJSONObject(T item);

    public T saveItem(T t) {
        t.setId(nextValue());
        getItems().add(t);
        save();
        return t;
    }

    public T saveEditedItem(T t, T oldT) {
        t.setId(oldT.getId());
        getItems().remove(oldT);
        getItems().add(t);
        save();
        return t;
    }

    public boolean deleteItem(T item) {
        getItems().remove(item);
        save();
        return true;
    }

    protected void calculateMaxId() {
        this.id = items.stream()
            .max(Comparator.comparing(T::getId))
            .map(t -> t.getId())
            .orElse(0L);
    }

    public Long nextValue() {
        id++;
        return id;
    }
}
