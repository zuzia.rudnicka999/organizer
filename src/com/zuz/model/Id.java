package com.zuz.model;

public interface Id {
    Long getId();

    void setId(Long id);
}
