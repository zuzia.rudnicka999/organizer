package com.zuz.views.monthwindow.spendinginmonth;

import java.util.List;
import java.util.Optional;

import com.zuz.database.DataSource;
import com.zuz.model.Spending;
import com.zuz.model.SpendingCategoryEnum;
import com.zuz.views.MainWindow;
import com.zuz.viewutil.GenericTable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class SpendingInMonthTable extends GenericTable<Spending> {

    private MainWindow owner;

    public SpendingInMonthTable(MainWindow owner) {
        this.owner = owner;
    }

    @Override
    protected String getTitle() {
        return "Wydatki";
    }

    @Override
    protected Optional<Spending> getEnteredData() { //add
        return null;
    }

    @Override
    protected Optional<Spending> getEnteredData(Spending spending) { //edit
        return null;
    }

    @Override
    public TableView<Spending> getTableView() {
        TableColumn<Spending, String> dateColumn = new TableColumn<>("Dzień");
        dateColumn.setMinWidth(100);
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));

        TableColumn<Spending, String> spendingColumn = new TableColumn<>("Wydatek");
        spendingColumn.setMinWidth(550);
        spendingColumn.setCellValueFactory(new PropertyValueFactory<>("spending"));

        TableColumn<Spending, String> amountColumn = new TableColumn<>("Kwota");
        amountColumn.setMinWidth(100);
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));

        TableColumn<Spending, SpendingCategoryEnum> categoryColumn = new TableColumn<>("Kategoria");
        categoryColumn.setMinWidth(200);
        categoryColumn.setCellValueFactory(
            cellData -> new SimpleObjectProperty(SpendingCategoryEnum.valueOf(cellData.getValue().getCategory().name()).getName()));

        TableView<Spending> tableView = new TableView<>();

        tableView.getColumns().addAll(dateColumn, amountColumn, spendingColumn, categoryColumn);
        return tableView;
    }

    @Override
    public List<Spending> getDataForDisplay() {
        return SpendingInMonthDataSource.loadForDisplay(owner.getDay());
    }

    @Override
    public DataSource getDataSource() {
        return new SpendingInMonthDataSource();
    }

    @Override
    public boolean findVisible() {
        return false;
    }

    @Override
    public ChangeListener<String> getStringChangeListener(FilteredList<Spending> filteredData) {
        return null;
    }

    @Override
    protected Parent getBottom() {
        return null;
    }

}
