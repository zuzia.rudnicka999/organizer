package com.zuz.views.monthwindow.importanttodo;

import java.util.Optional;

import com.zuz.model.PriorityEnum;
import com.zuz.model.ToDo;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ImportantToDoAdd {

    private Optional<ToDo> toDo = Optional.empty();

    private Stage window;

    private TextField txtToDo;

    private DatePicker dateInput;


    public ImportantToDoAdd() { //add
    }

    public ImportantToDoAdd(ToDo toDo) {
        this.toDo = Optional.of(toDo);
    } //edit

    public void initInputs() {
        toDo.ifPresent(toDo1 -> {
            this.txtToDo.setText(toDo1.getToDo());
            this.dateInput.setValue(toDo1.getDate());
        });
    }

    public Optional<ToDo> displayAdd() {

        window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);


        // Create the birth date label and datePicker field
        Label lblDate = new Label("Data");
        dateInput = new DatePicker();

        // Create the first name label and text field
        Label lblToDo = new Label("Do zrobienia:");
        txtToDo = new TextField();
        txtToDo.setMinWidth(60);
        txtToDo.setPrefWidth(200);
        txtToDo.setMaxWidth(280);
        txtToDo.setPromptText("Wprowadź co masz do zrobienia");


        // Create the buttons
        Button btnOK = new Button("OK");
        btnOK.setPrefWidth(80);
        btnOK.setOnAction(e -> btnOK_Click());

        btnOK.disableProperty().bind(
            txtToDo.textProperty().isEmpty()
                .or(dateInput.valueProperty().isNull())
        );

        Button btnCancel = new Button("Anuluj");
        btnCancel.setPrefWidth(80);
        btnCancel.setOnAction(e -> btnCancel_Click());
        HBox paneButtons = new HBox(10, btnOK, btnCancel);

        // Create the GridPane layout
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10));
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setMinWidth(500);
        grid.setPrefWidth(500);
        grid.setMaxWidth(800);
        // Add the nodes to the pane
        grid.addRow(0, lblDate, dateInput);
        grid.addRow(1, lblToDo, txtToDo);
        grid.add(paneButtons, 2, 5);

        // Set alignments and spanning
        grid.setHalignment(lblDate, HPos.RIGHT);
        grid.setHalignment(lblToDo, HPos.RIGHT);
        grid.setColumnSpan(dateInput, 2);
        grid.setColumnSpan(txtToDo, 2);

        // Set column widths
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(33);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(33);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(33);
        grid.getColumnConstraints().addAll(col1, col2, col3);

        // Create the scene and the stage
        Scene scene = new Scene(grid);
        window.setScene(scene);
        window.setTitle("Zadanie");
        window.setMinWidth(500);
        window.setMaxWidth(900);

        initInputs();

        window.showAndWait();

        return toDo;
    }

    private void btnCancel_Click() {
        toDo = Optional.empty();
        window.close();
    }

    private void btnOK_Click() {
        toDo = Optional
            .of(new ToDo(1L, dateInput.getValue(), txtToDo.getText(), PriorityEnum.BARDZO_WAZNE, Boolean.TRUE));
        window.close();
    }
}


