package com.zuz.views.monthwindow.importanttodo;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import com.zuz.database.DataSource;
import com.zuz.model.ToDo;
import com.zuz.views.daywindow.todo.ToDoBase;

public class ImportantToDoDataSource extends DataSource<ToDo> {

    public static List<ToDo> loadForDisplay(LocalDate day) {
        ToDoBase toDoBase = ToDoBase.getTable();
        return toDoBase
            .findAll()
            .stream()
            .filter(toDo -> (
                toDo.getDate().getMonthValue() == day.getMonthValue()
                    && toDo.getDate().getYear() == day.getYear()
                    && toDo.getExtra())
            ).collect(Collectors.toList());
    }

    @Override
    public ToDo addItem(ToDo toDo) {
        return ToDoBase.getTable().saveItem(toDo);
    }

    @Override
    public ToDo editItem(ToDo toDo, ToDo oldToDo) {
        toDo.setPriority(oldToDo.getPriority());
        toDo.setExtra(oldToDo.getExtra());
        return ToDoBase.getTable().saveEditedItem(toDo, oldToDo);
    }

    @Override
    public boolean deleteItem(ToDo toDo) {
        ToDoBase.getTable().deleteItem(toDo);
        return true;
    }

}
