package com.zuz.views.monthwindow;

import com.zuz.views.MainWindow;
import com.zuz.views.monthwindow.importanttodo.ImportantToDoTable;
import com.zuz.views.monthwindow.spendinginmonth.SpendingInMonthTable;
import com.zuz.views.monthwindow.spendingbycategory.SpendingByCategoryTable;
import com.zuz.viewutil.MakeWindowCenter;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class MonthWindow implements MakeWindowCenter {

    private MainWindow mainWindow;

    public MonthWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public Pane makeCenter() {
        GridPane monthGrid = new GridPane();
        monthGrid.setPadding(new Insets(10, 10, 10, 10));
        monthGrid.setVgap(8);
        monthGrid.setHgap(10);
        // Set column widths
        ColumnConstraints colTasks = new ColumnConstraints();
        colTasks.setPercentWidth(100);
        monthGrid.getColumnConstraints().addAll(colTasks);

        Parent monthTasks = getMonthTasks();
        GridPane.setConstraints(monthTasks, 0, 0);

        GridPane spendingsGrid = new GridPane();
        spendingsGrid.setPadding(new Insets(10, 10, 10, 10));
        spendingsGrid.setVgap(8);
        spendingsGrid.setHgap(10);

        ColumnConstraints colSpendings = new ColumnConstraints();
        colSpendings.setPercentWidth(60);
        ColumnConstraints colSpendingsByCategory = new ColumnConstraints();
        colSpendingsByCategory.setPercentWidth(40);
        spendingsGrid.getColumnConstraints().addAll(colSpendings, colSpendingsByCategory);
        Parent spendings = getSpendings();
        GridPane.setConstraints(spendings, 0, 0);
        Parent spendingsByCategory = getSpendingsByCategory();
        GridPane.setConstraints(spendingsByCategory, 1, 0);

        spendingsGrid.getChildren().addAll(spendings, spendingsByCategory);

        GridPane.setConstraints(spendingsGrid, 0, 1);
        monthGrid.getChildren().addAll(monthTasks, spendingsGrid);

        return monthGrid;
    }

    private Parent getMonthTasks() {
        ImportantToDoTable importantToDoTable = new ImportantToDoTable(mainWindow);
        return importantToDoTable.createView();
    }

    private Parent getSpendings() {
        SpendingInMonthTable spendingInMonthTable = new SpendingInMonthTable(mainWindow);
        return spendingInMonthTable.createView();
    }

    private  Parent getSpendingsByCategory() {
        SpendingByCategoryTable spendingByCategoryTable = new SpendingByCategoryTable(mainWindow);
        return spendingByCategoryTable.createView();
    }
}
