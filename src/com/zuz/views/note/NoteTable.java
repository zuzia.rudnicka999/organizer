package com.zuz.views.note;

import java.util.List;
import java.util.Optional;

import com.zuz.database.DataSource;
import com.zuz.model.Note;
import com.zuz.viewutil.GenericTable;
import javafx.beans.value.ChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class NoteTable extends GenericTable<Note> {

    @Override
    protected String getTitle() {
        return "Notatki";
    }

    @Override
    protected Optional<Note> getEnteredData() { //add
        NoteAdd noteAdd = new NoteAdd();
        return noteAdd.displayAdd();
    }

    @Override
    protected Optional<Note> getEnteredData(Note note) { //edit
        NoteAdd noteAdd = new NoteAdd(note);
        return noteAdd.displayAdd();
    }

    @Override
    public TableView<Note> getTableView() {
        TableColumn<Note, String> firstNameColumn = new TableColumn<>("Tytuł");
        firstNameColumn.setMinWidth(200);
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableView<Note> tableView = new TableView<>();

        tableView.getColumns().add(firstNameColumn);
        return tableView;
    }

    @Override
    public List<Note> getDataForDisplay() {
        return NoteDataSource.loadForDisplay();
    }

    @Override
    public ChangeListener<String> getStringChangeListener(FilteredList<Note> filteredData) {
        return (observable, oldValue, newValue) -> {
            filteredData.setPredicate(note -> {
                // If filter text is empty, display all notes.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare title with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (note.getTitle().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true; // Filter matches  name.
                }
                return false; // Does not match.
            });
        };
    }

    @Override
    public DataSource getDataSource() {
        return new NoteDataSource();
    }

}
