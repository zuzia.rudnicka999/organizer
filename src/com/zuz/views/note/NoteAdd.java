package com.zuz.views.note;

import java.util.Optional;

import com.zuz.model.Note;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class NoteAdd {

    private Optional<Note> note = Optional.empty();

    private Stage window;

    private TextField txtTitle;

    private TextArea txtContent;


    public NoteAdd() { //add
    }

    public NoteAdd(Note note) {
        this.note = Optional.of(note);
    } //edit

    public void initInputs() {
        note.ifPresent(note1 -> {
            this.txtTitle.setText(note.get().getTitle());
            this.txtContent.setText(note.get().getContent());
        });
    }

    public Optional<Note> displayAdd() {

        window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);

        Label lblTitle = new Label("Tytuł");
        txtTitle = new TextField();
        txtTitle.setMinWidth(60);
        txtTitle.setPrefWidth(120);
        txtTitle.setMaxWidth(180);
        txtTitle.setPromptText("Wprowadź tytuł");

        Label lblContent = new Label("Treść notatki");
        txtContent = new TextArea();
        txtContent.setMinWidth(100);
        txtContent.setPrefWidth(200);
        txtContent.setMaxWidth(300);

        // Create the buttons
        Button btnOK = new Button("OK");
        btnOK.setPrefWidth(80);
        btnOK.setOnAction(e -> btnOK_Click());

        btnOK.disableProperty().bind(
            txtTitle.textProperty().isEmpty()
                .or(txtContent.textProperty().isEmpty())
        );

        Button btnCancel = new Button("Anuluj");
        btnCancel.setPrefWidth(80);
        btnCancel.setOnAction(e -> btnCancel_Click());
        HBox paneButtons = new HBox(10, btnOK, btnCancel);

        // Create the GridPane layout
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10));
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setMinWidth(500);
        grid.setPrefWidth(500);
        grid.setMaxWidth(800);
        // Add the nodes to the pane
        grid.addRow(0, lblTitle, txtTitle);
        grid.addRow(1, lblContent, txtContent);
        grid.add(paneButtons, 2, 5);

        // Set alignments and spanning
        grid.setHalignment(lblTitle, HPos.RIGHT);
        grid.setHalignment(lblContent, HPos.RIGHT);
        grid.setColumnSpan(txtTitle, 2);
        grid.setColumnSpan(txtContent, 2);

        // Set column widths
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(33);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(33);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(33);
        grid.getColumnConstraints().addAll(col1, col2, col3);

        // Create the scene and the stage
        Scene scene = new Scene(grid);
        window.setScene(scene);
        window.setTitle("Notatka");
        window.setMinWidth(500);
        window.setMaxWidth(900);

        initInputs();

        window.showAndWait();

        return note;
    }

    private void btnCancel_Click() {
        note = Optional.empty();
        window.close();
    }

    private void btnOK_Click() {
        note = Optional
            .of(new Note(1L, txtTitle.getText(), txtContent.getText()));
        window.close();
    }
}


