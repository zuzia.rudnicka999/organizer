package com.zuz.views;

import java.time.LocalDate;
import java.util.HashMap;

import com.zuz.util.DaysList;
import com.zuz.util.MonthMapper;
import com.zuz.viewutil.MakeWindowCenter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class YearWindow implements MakeWindowCenter {

    private Integer year;
    private Integer month;

    private MainWindow mainWindow;

    private BorderPane borderPane;

    private Button buttonYear;

    private HashMap<Integer, Button> monthButtons;

    public YearWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public Pane makeCenter() {
        this.year = mainWindow.getYear();

        borderPane = new BorderPane();
        borderPane.setLeft(getMonthMenu());
        setSelectedMonth(mainWindow.getMonth());

        borderPane.setCenter(getCalendar());
        return borderPane;
    }

    private GridPane getMonthMenu() {
        monthButtons = new HashMap<>();
        GridPane monthMenu = new GridPane();
        monthMenu.setPadding(new Insets(10, 10, 10, 10));
        monthMenu.setVgap(8);
        monthMenu.setHgap(10);
        MonthMapper.months.forEach((monthNumber, monthName) -> {
            Button button = new Button(monthName);
            button.setStyle("-fx-background-color: #d3d3d3;");
            button.setPrefWidth(150);
            button.setOnAction(getMonthActionEventHandler());
            GridPane.setConstraints(button, 0, monthNumber + 5);
            monthMenu.getChildren().add(button);
            monthButtons.put(monthNumber, button);
        });
        return monthMenu;
    }

    private void setSelectedMonth(Integer selectedMonth) {
        if (month != null) {
            Button prevSelectedMonthButton = monthButtons.get(month);
            prevSelectedMonthButton.setStyle("-fx-background-color: #d3d3d3;");
        }
        month = selectedMonth;
        Button monthButton = monthButtons.get(selectedMonth);
        monthButton.setStyle("-fx-background-color: #add8e6;");
    }

    private BorderPane getCalendar() {

        BorderPane borderPane = new BorderPane();
        GridPane yearGrid = new GridPane();
        yearGrid.setPadding(new Insets(10, 10, 10, 10));
        yearGrid.setVgap(8);
        yearGrid.setHgap(10);

        // Set column widths
        ColumnConstraints colBeforAnAfter = new ColumnConstraints();
        colBeforAnAfter.setPercentWidth(35);
        ColumnConstraints colIncAndDec = new ColumnConstraints();
        colIncAndDec.setPercentWidth(10);
        ColumnConstraints colYear = new ColumnConstraints();
        colYear.setPercentWidth(20);
        yearGrid.getColumnConstraints().addAll(colBeforAnAfter, colIncAndDec, colYear, colIncAndDec, colBeforAnAfter);

        Button buttonPrevious = new Button("<");
        buttonPrevious.setStyle("-fx-background-color: #d3d3d3;");
        buttonYear = new Button(year.toString());
        buttonYear.setPrefWidth(100);

        buttonPrevious.setOnAction(e -> {
            this.year = this.year - 1;
            buttonYear.setText(this.year.toString());
            borderPane.setCenter(getCalendarGrid());
        });
        Button buttonNext = new Button(">");
        buttonNext.setStyle("-fx-background-color: #d3d3d3;");
        buttonNext.setOnAction(e -> {
            this.year = this.year + 1;
            buttonYear.setText(this.year.toString());
            borderPane.setCenter(getCalendarGrid());
        });

        GridPane.setConstraints(buttonPrevious, 1, 0);
        GridPane.setConstraints(buttonYear, 2, 0);
        GridPane.setConstraints(buttonNext, 3, 0);
        yearGrid.getChildren().addAll(buttonPrevious, buttonYear, buttonNext);

        borderPane.setTop(yearGrid);
        borderPane.setCenter(getCalendarGrid());

        return borderPane;
    }

    private EventHandler<ActionEvent> getMonthActionEventHandler() {
        return e -> {
            String monthName = ((Button) e.getSource()).getText();
            Integer month = MonthMapper.monthNumbers.get(monthName);
            setSelectedMonth(month);
            borderPane.setCenter(getCalendar());
        };
    }

    private GridPane getCalendarGrid() {
        GridPane calendarGrid = new GridPane();
        calendarGrid.setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: gray;");
        calendarGrid.setPadding(new Insets(10, 10, 10, 10));
        calendarGrid.setVgap(8);
        calendarGrid.setHgap(10);

        // Set column widths
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(14);
        calendarGrid.getColumnConstraints().addAll(col1, col1, col1, col1, col1, col1, col1);

        //Nazwy dni tygodnia
        for (int i = 0; i < DaysList.days.size(); i++) {
            Label labelDay = new Label(DaysList.days.get(i));
            GridPane.setConstraints(labelDay, i, 0);
            calendarGrid.getChildren().add(labelDay);
        }

        LocalDate firstDayOfMonth = LocalDate.of(year, month, 1);
        int ofset = firstDayOfMonth.getDayOfWeek().getValue() - 1;
        int daysOfMonth = firstDayOfMonth.lengthOfMonth();
        int dayNumber = 1;
        for (int i = ofset; i < daysOfMonth + ofset; i++) {
            Button button = new Button(Integer.valueOf(dayNumber).toString());
            dayNumber++;
            button.setPrefWidth(50);
            button.setOnAction(e -> {
                LocalDate dateToSet = LocalDate.of(year, month, Integer.valueOf(((Button) e.getSource()).getText()));
                mainWindow.setDay(dateToSet);
                mainWindow.setCenter(mainWindow.getDayWindow().makeCenter());

            });
            GridPane.setConstraints(button, i % 7, i / 7 + 1);
            calendarGrid.getChildren().add(button);
        }

        return calendarGrid;
    }
}
