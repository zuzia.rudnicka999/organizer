package com.zuz.views.daywindow.todo;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import com.zuz.model.PriorityEnum;
import com.zuz.model.ToDo;
import com.zuz.viewutil.PriorityCellFactory;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ToDoAdd {

    private LocalDate day;

    private Optional<ToDo> toDo = Optional.empty();

    private Stage window;

    private TextField txtToDo;

    private ComboBox prioritizeBox;

    public ToDoAdd(LocalDate day) { //add
        this.day = day;
    }

    public ToDoAdd(ToDo toDo) {
        this.toDo = Optional.of(toDo);
    } //edit

    public void initInputs() {
        toDo.ifPresent(toDo1 -> {
            this.txtToDo.setText(toDo1.getToDo());
            this.prioritizeBox.setValue(toDo1.getPriority());
        });
    }

    public Optional<ToDo> displayAdd() {

        window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);

        // Create the first name label and text field
        Label lblToDo = new Label("Do zrobienia:");
        txtToDo = new TextField();
        txtToDo.setMinWidth(60);
        txtToDo.setPrefWidth(200);
        txtToDo.setMaxWidth(280);
        txtToDo.setPromptText("Wprowadź co masz do zrobienia");

        Label labelPrioritize = new Label("Priorytet zadania: ");
        prioritizeBox = new ComboBox<>();
        prioritizeBox.setMinWidth(100);
        prioritizeBox.setPrefWidth(200);
        prioritizeBox.setMaxWidth(300);
        prioritizeBox.getItems().addAll(Arrays.asList(PriorityEnum.values()));
        PriorityCellFactory priorityCellFactory = new PriorityCellFactory();
        prioritizeBox.setButtonCell(priorityCellFactory.call(null));
        prioritizeBox.setCellFactory(priorityCellFactory);
        prioritizeBox.setPromptText("Podaj priorytet");

        // Create the buttons
        Button btnOK = new Button("OK");
        btnOK.setPrefWidth(80);
        btnOK.setOnAction(e -> btnOK_Click());

        btnOK.disableProperty().bind(
            txtToDo.textProperty().isEmpty()
                .or(prioritizeBox.valueProperty().isNull())
        );

        Button btnCancel = new Button("Anuluj");
        btnCancel.setPrefWidth(80);
        btnCancel.setOnAction(e -> btnCancel_Click());
        HBox paneButtons = new HBox(10, btnOK, btnCancel);

        // Create the GridPane layout
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10));
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setMinWidth(500);
        grid.setPrefWidth(500);
        grid.setMaxWidth(800);
        // Add the nodes to the pane
        grid.addRow(0, lblToDo, txtToDo);
        grid.addRow(1, labelPrioritize, prioritizeBox);
        grid.add(paneButtons, 2, 5);

        // Set alignments and spanning
        grid.setHalignment(lblToDo, HPos.RIGHT);
        grid.setHalignment(labelPrioritize, HPos.RIGHT);
        grid.setColumnSpan(txtToDo, 2);
        grid.setColumnSpan(prioritizeBox, 2);

        // Set column widths
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(33);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(33);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(33);
        grid.getColumnConstraints().addAll(col1, col2, col3);

        // Create the scene and the stage
        Scene scene = new Scene(grid);
        window.setScene(scene);
        window.setTitle("Zadanie");
        window.setMinWidth(500);
        window.setMaxWidth(900);

        initInputs();

        window.showAndWait();

        return toDo;
    }

    private void btnCancel_Click() {
        toDo = Optional.empty();
        window.close();
    }

    private void btnOK_Click() {
        toDo = Optional
            .of(new ToDo(1L, day, txtToDo.getText(), (PriorityEnum) prioritizeBox.getValue(), Boolean.FALSE));
        window.close();
    }
}


