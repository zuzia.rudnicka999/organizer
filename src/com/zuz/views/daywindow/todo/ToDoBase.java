package com.zuz.views.daywindow.todo;

import java.time.LocalDate;

import org.json.simple.JSONObject;
import com.zuz.database.TableBase;
import com.zuz.model.PriorityEnum;
import com.zuz.model.ToDo;

public class ToDoBase extends TableBase<ToDo> {

    private static ToDoBase table;

    private ToDoBase() {
    }

    public static ToDoBase getTable() {
        if (table == null) {
            table = new ToDoBase();
            table.load();
        }
        return table;
    }

    @Override
    protected String tableName() {
        return "toDo";
    }


    public ToDo findById(Long id) {
        return table.getItems()
            .stream()
            .filter(toDo1 -> toDo1.getId().equals(id)).findAny().get();
    }

    @Override
    protected JSONObject mapToJSONObject(ToDo toDo) {
        JSONObject toDoDetails = new JSONObject();
        toDoDetails.put("id", toDo.getId());
        toDoDetails.put("date", toDo.getDate().toString());
        toDoDetails.put("toDo", toDo.getToDo());
        toDoDetails.put("priority", toDo.getPriority().toString());
        toDoDetails.put("extra", toDo.getExtra().toString());

        JSONObject toDoObject = new JSONObject();
        toDoObject.put(tableName(), toDoDetails);
        return toDoObject;
    }

    @Override
    protected ToDo parseItemJSONObject(JSONObject toDoJSON) {
        JSONObject toDoObject = (JSONObject) toDoJSON.get(tableName());

        Long id = (Long) toDoObject.get("id");
        LocalDate date = LocalDate.parse((String) toDoObject.get("date"));
        String toDoAction = (String) toDoObject.get("toDo");
        PriorityEnum priority = PriorityEnum.valueOf((String) toDoObject.get("priority"));
        Boolean extra = Boolean.valueOf((String) toDoObject.get("extra"));

        ToDo toDo = new ToDo(id, date, toDoAction, priority, extra);
        return toDo;
    }

}
